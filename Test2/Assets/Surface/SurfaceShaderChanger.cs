﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceShaderChanger : MonoBehaviour {

    Material myMaterial;

    Shader mainShader;
    [SerializeField]
    Shader collisionShader;
    [SerializeField]
    float collisionShaderTimer = 2f;

	void Start () {
        MeshRenderer renderer = GetComponentInChildren<MeshRenderer>(true);
        if(renderer != null)
        {
            myMaterial = renderer.material;
            mainShader = myMaterial.shader;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(myMaterial != null && collisionShader != null)
        {
            StartCoroutine(ApplyCollisionShader());
        }
    }

    IEnumerator ApplyCollisionShader()
    {
        myMaterial.shader = collisionShader;

        yield return new WaitForSeconds(collisionShaderTimer);

        myMaterial.shader = mainShader;
    }

}
