﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceSound : MonoBehaviour {

    AudioSource audioSource;

    [SerializeField]
    List<AudioClipVolume> clips;

    [SerializeField]
    bool isEnabled = true;

    bool IsEnabled
    {
        get
        {
            return SoundController.globalSurfaceSoundState && isEnabled;
        }
    }

    [SerializeField]
    float maxImpulse = 100f;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        PlaySound(collision);
    }

    public void PlaySound(Collision collision)
    {
        if (!IsEnabled)
            return;

        int randomClipIndex = Random.Range(0, clips.Count);

        AudioClipVolume curRandClip = clips[randomClipIndex];

        float curVolume = Mathf.Clamp(collision.impulse.magnitude / maxImpulse, 0f, 1f);

        audioSource.PlayOneShot(curRandClip.clip, curRandClip.volume * curVolume);
    }

}
