﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour {

    [SerializeField]
    bool enableCannonballSound = true;
    [SerializeField]
    bool enableSurfaceSound = true;

    public static bool globalCannonballSoundState { get; private set; }
    public static bool globalSurfaceSoundState { get; private set; }

    private void OnGUI()
    {
        globalCannonballSoundState = enableCannonballSound;
        globalSurfaceSoundState = enableSurfaceSound;
    }

}
