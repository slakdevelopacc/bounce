﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AudioClipVolume
{
    public AudioClip clip;
    public float volume;
}

[RequireComponent(typeof(AudioSource))]
public class CannonballSound : MonoBehaviour {

    AudioSource audioSource;

    [SerializeField]
    List<AudioClipVolume> clips;

    bool IsEnabled { get
        {
            return SoundController.globalCannonballSoundState;
        }
    }

    [SerializeField]
    float maxImpulse = 100f;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        PlaySound(collision);
    }

    void PlaySound(Collision collision)
    {
        if (!IsEnabled)
            return;

        int randomClipIndex = Random.Range(0, clips.Count);

        AudioClipVolume curRandClip = clips[randomClipIndex];

        float curVolume = Mathf.Clamp(collision.impulse.magnitude / maxImpulse, 0f, 1f);

        audioSource.PlayOneShot(curRandClip.clip, curRandClip.volume * curVolume);
    }

}
