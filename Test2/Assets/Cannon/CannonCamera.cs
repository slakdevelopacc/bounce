﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonCamera : MonoBehaviour {

    [SerializeField]
    Camera myCam;

	void Start () {
        myCam = GetComponentInChildren<Camera>(true);

        TrackingCamera.StartTrackingEvent += DisableCam;
        CannonShooter.ResetCannonballEvent += EnableCam;
    }

    void DisableCam()
    {
        myCam.enabled = false;
    }
	
    void EnableCam()
    {
        myCam.enabled = true;

    }

}
