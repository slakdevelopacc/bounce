﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonRotator : MonoBehaviour {

    [SerializeField]
    float rotateSpeed = 10f;

    //Используется для ограничения вертикального поворота
    Vector3 curEul;

    private void Start()
    {
        curEul = transform.eulerAngles;

        TouchRotateScript.DragEvent += Rotate;
    }

    void Rotate(Vector2 rotateDelta)
    {
        rotateDelta *= rotateSpeed * Time.deltaTime;
        RotateHorizontal(rotateDelta.x);
        RotateVertical(rotateDelta.y);
    }

    void RotateHorizontal(float angle)
    {
        transform.Rotate(Vector3.up, angle, Space.World);
    }

    void RotateVertical(float angle)
    {
        angle = Mathf.Clamp(angle + curEul.x, -90f, 90f) - curEul.x;
        transform.Rotate(transform.right, -angle, Space.World);
        curEul.x += angle;
    }
}
