﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonShooter : MonoBehaviour {

    [SerializeField]
    GameObject cannonballPrefab;
    GameObject spawnedCannonball;

    [SerializeField]
    float offsetFromCenter = 1f;
    [SerializeField]
    float cannonPower = 999f;

    [SerializeField]
    float cannonMinPower = 10f;
    [SerializeField]
    float cannonMaxPower = 1000f;

    [SerializeField]
    KeyCode shootKeyCode = KeyCode.B;
    [SerializeField]
    KeyCode resetKeyCode = KeyCode.N;

    public static event System.Action ShootEvent;
    public static event System.Action ResetCannonballEvent;

    private void Update()
    {
        if(Input.GetKeyDown(shootKeyCode))
        {
            Shoot();
        }

        if(Input.GetKeyDown(resetKeyCode))
        {
            ResetCannonball();
        }
    }

    public void Shoot()
    {
        if (spawnedCannonball == null)
        {
            if (ShootEvent != null)
            {
                ShootEvent();
            }

            spawnedCannonball = Instantiate(cannonballPrefab, transform.position + transform.forward * offsetFromCenter, transform.rotation) as GameObject;
            Rigidbody cannonballRigidbody = spawnedCannonball.GetComponent<Rigidbody>();
            cannonballRigidbody.AddForce(transform.forward * cannonPower);
        }
    }

    public void Shoot(float power)
    {
        float prevPower = cannonPower;

        SetPower(power);

        Shoot();

        cannonPower = prevPower;
    }

    public void ResetCannonball()
    {
        if(ResetCannonballEvent != null)
        {
            ResetCannonballEvent();
        }

        Destroy(spawnedCannonball);
        spawnedCannonball = null;
    }

    public void SetPower(float power)
    {
        cannonPower = (cannonMaxPower - cannonMinPower) * power;
    }

    public float GetPower()
    {
        return cannonPower / (cannonMaxPower - cannonMinPower);
    }

    private void OnDrawGizmos()
    {
        if (cannonballPrefab != null)
        {
            MeshFilter cannonballMeshFilter = cannonballPrefab.GetComponentInChildren<MeshFilter>();
            if (cannonballMeshFilter != null)
            {
                Mesh cannonballMesh = cannonballMeshFilter.sharedMesh;
                if (cannonballMesh != null)
                {
                    Gizmos.color = new Color(1f,0f,0f, 0.6f);
                    Gizmos.DrawWireMesh(cannonballMesh, transform.position + transform.forward * offsetFromCenter);
                }
            }
        }
    }

}
