﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shoot2Button : MonoBehaviour {

    bool isCharging = false;
    float curCharge = 0f;
    [SerializeField]
    float chargeSpeed = 0.2f;

    [SerializeField]
    CannonShooter cannonShooter;

    [SerializeField]
    Image chargeImg1;
    [SerializeField]
    Image chargeImg2;

    [SerializeField]
    Slider chargeSlider;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
        if(isCharging)
        {
            curCharge = Mathf.Clamp(curCharge + chargeSpeed * Time.deltaTime, 0f, 1f);
            chargeSlider.value = curCharge;
            chargeImg1.color = new Color(1f, 1f-curCharge, 1f - curCharge);
            chargeImg2.color = new Color(1f, 1f-curCharge, 1f - curCharge);
        }
	}

    public void OnPointerDown()
    {
        isCharging = true;
    }

    public void OnPointerUp()
    {
        cannonShooter.Shoot(curCharge);

        curCharge = 0f;
        chargeSlider.value = curCharge;
        chargeImg1.color = new Color(1f, 1f - curCharge, 1f - curCharge);
        chargeImg2.color = new Color(1f, 1f - curCharge, 1f - curCharge);

        isCharging = false;
    }
}
