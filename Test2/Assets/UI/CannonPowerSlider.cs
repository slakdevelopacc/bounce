﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CannonPowerSlider : MonoBehaviour {

    [SerializeField]
    CannonShooter cannonShooter;
    [SerializeField]
    Slider slider;

    private void Start()
    {
        slider.value = cannonShooter.GetPower();
    }

    public void SetPower()
    {
        cannonShooter.SetPower(slider.value);
    }

}
