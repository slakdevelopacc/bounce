﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchRotateScript : MonoBehaviour {

    public static event System.Action<Vector2> DragEvent;

    public void OnDrag(BaseEventData eventData)
    {
        PointerEventData ped = eventData as PointerEventData;

        if(DragEvent != null)
        {
            DragEvent(ped.delta);
        }
    }
}
