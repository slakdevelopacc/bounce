﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingCamera : MonoBehaviour {

    Camera myCam;

    [SerializeField]
    List<TrackingTriggerTag> trackingTriggers;

    static List<TrackingCamera> trackingCams = new List<TrackingCamera>();

    bool isTrackingOn = false;

    public static event System.Action StartTrackingEvent;

	void Start () {
        myCam = GetComponentInChildren<Camera>(true);

        foreach (var trigger in trackingTriggers)
        {
            trigger.TriggerEnterEvent += OnTrackingTriggerEnter;
        }

        trackingCams.Add(this);

        CannonShooter.ShootEvent += EnableTracking;
        CannonShooter.ResetCannonballEvent += DisableTracking;

        DisableCam();
    }

    void EnableTracking()
    {
        isTrackingOn = true;
    }

    void DisableTracking()
    {
        isTrackingOn = false;
    }

    void OnTrackingTriggerEnter()
    {
        if (!isTrackingOn)
            return;

        EnableCam();

        foreach (var cam in trackingCams)
        {
            if(cam != this)
            {
                cam.DisableCam();
            }
        }
    }

    public void DisableCam()
    {
        myCam.enabled = false;
    }

    public void EnableCam()
    {
        myCam.enabled = true;
        if(StartTrackingEvent != null)
        {
            StartTrackingEvent();
        }
    }

}
