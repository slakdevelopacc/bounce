﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackingTriggerTag : MonoBehaviour {

    [SerializeField]
    LayerMask trackingMask;

    public event System.Action TriggerEnterEvent;

    private void OnTriggerEnter(Collider other)
    {
        if (((1 << other.gameObject.layer) & trackingMask.value) != 0)
        {
            if (TriggerEnterEvent != null)
            {
                TriggerEnterEvent();
            }
        }
    }
}
